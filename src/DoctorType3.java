import java.util.Random;

public class DoctorType3 extends Doctor {
    public DoctorType3(String firstName, String lastName){
        super(firstName, lastName);
    }
    public void setBloodPressure1(Patient p){
        //set initial blood pressure
        Random rand = new Random();
        //generate a random number between 50 and 200
        int bloodPressure = rand.nextInt(50 + 1) + 200;
        p.setInitialBloodPressure(bloodPressure, this);
    }
    public void setBloodPressure2(Patient p){
        //set blood pressure after treatment
        Random rand = new Random();
        //generate a random number between 50 and 200
        int bloodPressure = rand.nextInt(50 + 1) + 200;
        p.setBloodPressureAfterTreatment(bloodPressure, this);
    }
}
