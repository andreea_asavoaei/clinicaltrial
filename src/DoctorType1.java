import java.util.Random;

public class DoctorType1 extends Doctor {
    public DoctorType1(String firstName, String lastName){
        super(firstName, lastName);
    }
    public void setRandomizationNumber(Patient p){
        //check if the patient doesn't have the unique number assigned
        if(!p.hasNumberAssigned()) {
            Random rand = new Random();
            //generate a random number
            int number = rand.nextInt(5000);
            //test if the generated number has already been assigned
            //assigned = exists in the hashset of assigned numbers for patients
            //if it has been assigned, then continue generating
            while (TrialSimulation.getNumbers().contains(number)) {
                number = rand.nextInt(5000);
            }
            //set the random number and add it to the hashset
            p.setNumber(number);
            TrialSimulation.getNumbers().add(number);
        }
    }

}
