public class Patient extends Person implements Comparable<Patient>{
    private int number;
    private Treatment treatment;
    private int initialBloodPressure;
    private int bloodPressureAfterTreatment;
    private DoctorType3 doctor1;
    private DoctorType2 doctor2;
    private DoctorType3 doctor3;
    public Patient(String firstName, String lastName){
        super(firstName, lastName);
    }

    public void setNumber(int number){
        this.number = number;
    }
    public int getNumber(){
        return number;
    }
    public DoctorType3 getDoctor1(){
        return doctor1;
    }
    public DoctorType2 getDoctor2(){
        return doctor2;
    }
    public DoctorType3 getDoctor3(){
        return doctor3;
    }
    public void setTreatment(Treatment treatment, DoctorType2 doctor2){
        this.treatment = treatment;
        this.doctor2 = doctor2;
    }
    public Treatment getTreatment(){
        return treatment;
    }
    public void setInitialBloodPressure(int initialBloodPressure, DoctorType3 doctor1){
        this.initialBloodPressure = initialBloodPressure;
        this.doctor1 = doctor1;
    }
    public int getInitialBloodPressure(){
        return initialBloodPressure;
    }
    public void setBloodPressureAfterTreatment(int bloodPressureAfterTreatment, DoctorType3 doctor3){
        this.bloodPressureAfterTreatment = bloodPressureAfterTreatment;
        this.doctor3 = doctor3;
    }
    public int getBloodPressureAfterTreatment(){
        return bloodPressureAfterTreatment;
    }
    public boolean hasNumberAssigned(){
        return number > 0;
    }
    public boolean hasTreatment(){ return String.valueOf(treatment) != null; }
    public String treatmentIsSuccessful(){
        if(initialBloodPressure > bloodPressureAfterTreatment){
            return "OK";
        }
        return "NOT OK";
    }
    @Override
    public int compareTo(Patient o) {
        if(this.getLastName().equals(o.getLastName())){
            return this.getFirstName().compareTo(o.getFirstName());
        }
        return this.getLastName().compareTo(o.getLastName());
    }
}
