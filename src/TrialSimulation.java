
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.StreamSupport;

public class TrialSimulation {
    private static HashSet<Integer> numbers = new HashSet<>();
    public static HashSet<Integer> getNumbers(){
        return numbers;
    }

    public static void main(String[] args){
        DoctorType1[] doctors1 = new DoctorType1[2];
        doctors1[0] = new DoctorType1("Ion", "Ionescu");
        doctors1[1] = new DoctorType1("Maria", "Dumitrescu");
        DoctorType2[] doctors2 = new DoctorType2[2];
        doctors2[0] = new DoctorType2("Ramona", "Sima");
        doctors2[1] = new DoctorType2("Gheorghe", "Popescu");
        DoctorType3[] doctors3 = new DoctorType3[2];
        doctors3[0] = new DoctorType3("Alexandru", "Ivan");
        doctors3[1] = new DoctorType3("Roberta", "Cristea");
        List<Patient> patients = new ArrayList<>();
        Patient patient1 = new Patient("Andreea", "Tudor");
        Patient patient2 = new Patient("Stefan", "Borza");
        Patient patient3 = new Patient("Silvia", "Ion");
        Patient patient4 = new Patient("Raluca", "Rusu");
        Patient patient5 = new Patient("Marin", "Vasilescu");
        Patient patient6 = new Patient("Florin", "Costea");
        Patient patient7 = new Patient("Alina", "Zimcea");
        Patient patient8 = new Patient("Maria", "Kovacs");
        Patient patient9 = new Patient("Tudor", "Martin");
        Patient patient10 = new Patient("Elena", "Pop");
        //set randomization numbers for patients
        doctors1[0].setRandomizationNumber(patient1);
        doctors1[0].setRandomizationNumber(patient2);
        doctors1[0].setRandomizationNumber(patient3);
        doctors1[0].setRandomizationNumber(patient4);
        doctors1[0].setRandomizationNumber(patient5);
        doctors1[1].setRandomizationNumber(patient6);
        doctors1[1].setRandomizationNumber(patient7);
        doctors1[1].setRandomizationNumber(patient8);
        doctors1[1].setRandomizationNumber(patient9);
        doctors1[1].setRandomizationNumber(patient10);
        //set initial blood pressure
        doctors3[0].setBloodPressure1(patient1);
        doctors3[0].setBloodPressure1(patient2);
        doctors3[0].setBloodPressure1(patient3);
        doctors3[0].setBloodPressure1(patient4);
        doctors3[0].setBloodPressure1(patient5);
        doctors3[1].setBloodPressure1(patient6);
        doctors3[1].setBloodPressure1(patient7);
        doctors3[1].setBloodPressure1(patient8);
        doctors3[1].setBloodPressure1(patient9);
        doctors3[1].setBloodPressure1(patient10);
        //set treatment for patients
        doctors2[0].setTreatment(patient1);
        doctors2[0].setTreatment(patient2);
        doctors2[0].setTreatment(patient3);
        doctors2[0].setTreatment(patient4);
        doctors2[0].setTreatment(patient5);
        doctors2[1].setTreatment(patient6);
        doctors2[1].setTreatment(patient7);
        doctors2[1].setTreatment(patient8);
        doctors2[1].setTreatment(patient9);
        doctors2[1].setTreatment(patient10);
        //set blood pressure after treatment
        doctors3[0].setBloodPressure2(patient1);
        doctors3[0].setBloodPressure2(patient2);
        doctors3[0].setBloodPressure2(patient3);
        doctors3[0].setBloodPressure2(patient4);
        doctors3[0].setBloodPressure2(patient5);
        doctors3[1].setBloodPressure2(patient6);
        doctors3[1].setBloodPressure2(patient7);
        doctors3[1].setBloodPressure2(patient8);
        doctors3[1].setBloodPressure2(patient9);
        doctors3[1].setBloodPressure2(patient10);
        patients.add(patient1);
        patients.add(patient2);
        patients.add(patient3);
        patients.add(patient4);
        patients.add(patient5);
        patients.add(patient6);
        patients.add(patient7);
        patients.add(patient8);
        patients.add(patient9);
        patients.add(patient10);
        Collections.sort(patients);
        boolean success = true;
        for(Patient p:patients){
            System.out.println("Patient " + p + " has been randomized with number " + p.getNumber() +
                    ". Doctor " + p.getDoctor1() + " recorded blood pressure before treatment with value " + p.getInitialBloodPressure() +
                    ". He has administered a " + p.getTreatment() + " treatment by doctor " + p.getDoctor2() +
                    ". Doctor " + p.getDoctor3() + " recorded blood pressure after treatment with value " + p.getBloodPressureAfterTreatment() +
                    ". Conclusion " + p.treatmentIsSuccessful());
            if(p.treatmentIsSuccessful().equals("NOT OK")){
                success = false;
            }
        }
        System.out.println("Trial success: " + success);
    }
}
