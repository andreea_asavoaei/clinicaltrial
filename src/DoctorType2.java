public class DoctorType2 extends Doctor {
    public DoctorType2(String firstName, String lastName){
        super(firstName, lastName);
    }
    public void setTreatment(Patient p){
        if(p.hasNumberAssigned()){
            if(p.getNumber()%2 == 0){
                p.setTreatment(Treatment.REAL, this);
            }else{
                p.setTreatment(Treatment.PLACEBO, this);
            }
        }
    }

}
